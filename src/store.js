import Vue from 'vue';
import Vuex from 'vuex';

import auth from '@/store/auth';
import player from '@/store/player';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    player
  }
});
