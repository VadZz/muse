import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/home',
    },
    {
      path: '/home',
      name: 'home',
      component: () => import(/* webpackChunkName: "home" */ './views/Home.vue')
    },
    {
      path: '/library',
      name: 'library',
      component: () => import(/* webpackChunkName: "library" */ './views/Library.vue')
    },
    {
      path: '/artist/:id',
      name: 'artist',
      component: () => import(/* webpackChunkName: "artist" */ './views/Artist.vue')
    },
    {
      path: '/top/:id',
      name: 'top',
      component: () => import(/* webpackChunkName: "top" */ './views/Top.vue')
    }
  ]
})
