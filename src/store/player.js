export default {
  namespaced: true,
  state: {
    playing: null,
    paused: false,
    queue: []
  },
  mutations: {
    setPlaying(state, { track }) {
      state.playing = track;
    },
    clearQueue(state) {
      state.playing = null,
      state.queue = [];
    },
    addToQueue(state, { tracks }) {
      state.queue.push(...tracks);
    },
    setPaused(state, { paused }) {
      state.paused = paused;
    }
  },
  actions: {
    play(ctx) {
      ctx.commit('setPaused', { paused: false });
      window.dispatchEvent(new CustomEvent('play'));
    },
    pause(ctx) {
      ctx.commit('setPaused', { paused: true });
      window.dispatchEvent(new CustomEvent('pause'));
    },
    unpause(ctx) {
      ctx.commit('setPaused', { paused: false });
      window.dispatchEvent(new CustomEvent('unpause'));
    },
    next(ctx) {
      if (ctx.state.queue.length > 0) {
        ctx.commit('setPlaying', { track: ctx.state.queue.shift() });
        ctx.dispatch('play');
      } else {
        ctx.commit('setPlaying', { track: null });
      }
    },
    playNext(ctx, { tracks }) {
      ctx.commit('addToQueue', { tracks });

      if (ctx.state.playing == null) {
        ctx.dispatch('next');
      }
    },
    playNow(ctx, { tracks }) {
      ctx.commit('clearQueue');
      ctx.dispatch('playNext', { tracks });
    }
  }
}
