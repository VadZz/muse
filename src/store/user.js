import axios from 'axios';
import config from '@/config.json';

export default {
  namespaced: true,
  state: {
    playlists: []
  },
  mutations: {

  },
  actions: {
    async updatePlaylists(ctx) {
      const token = ctx.rootState.auth.token;

      if (token) {
        try {
          const result = await axios.get(config.apiURL + '/streaming/playlists/', {
            headers: {
              Authorization: `JWT ${token}`
            }
          });
          
          console.log(result);
        } catch (e) {
          console.error(e);
        }
      }
    }
  }
}
