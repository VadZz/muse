import axios from 'axios';
import config from '@/config.json';
import Cookies from 'browser-cookies';

export default {
  namespaced: true,
  state: {
    artists: {

    }
  },
  mutations: {
    setArtists(state, { token }) {
      state.token = token;
    }
  },
  actions: {
    async loadArtists(ctx) {
      const token = Cookies.get('token');

      if (token) {
        try {
          const result = await axios.post(config.apiURL + '/token-verify/', { token });
          console.log(result);

          ctx.commit('setToken', { token });
          ctx.commit('setUser', { user: result.data.user_data });

        } catch (e) {
          console.error(e);
        }
      }
    }
  }
}
