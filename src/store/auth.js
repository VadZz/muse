import axios from 'axios';
import config from '@/config.json';
import Cookies from 'browser-cookies';

export default {
  namespaced: true,
  state: {
    token: null,
    user: null,
    playlists: []
  },
  mutations: {
    setToken(state, { token }) {
      state.token = token;
    },
    setUser(state, { user }) {
      state.user = user;
    },
    setPlaylists(state, { playlists }) {
      state.playlists = playlists;
    }
  },
  actions: {
    async loadPlaylists(ctx) {
      try {
        const result = await axios.get(config.apiURL + '/streaming/playlists/', {
          headers: {
            'Authorization': `JWT ${ctx.state.token}`
          }
        });

        if (!result.data.error) {
          ctx.commit('setPlaylists', { playlists: result.data.content.playlists });
        } else {
          throw result.data.error_text;
        }
      } catch (e) {
        console.error(e);
      }
    },
    async saveTrack(ctx, { playlist, track }) {
      try {
        const params = {};

        params.track_id = track;

        if (playlist) {
          params.playlist_id = playlist;
        }

        console.log(params);

        const result = await axios.post(config.apiURL + '/streaming/save-track/', params, {
          headers: {
            'Authorization': `JWT ${ctx.state.token}`
          }
        });

        if (!result.data.error) {
          ctx.dispatch('loadPlaylists');
        } else {
          throw result.data.error_text;
        }
      } catch (e) {
        console.error(e);
      }
    },
    async saveAlbum(ctx, { playlist, album }) {
      try {
        const params = {};

        params.album_id = album;

        if (playlist) {
          params.playlist_id = playlist;
        }

        console.log(params);

        const result = await axios.post(config.apiURL + '/streaming/save-album/', params, {
          headers: {
            'Authorization': `JWT ${ctx.state.token}`
          }
        });

        if (!result.data.error) {
          ctx.dispatch('loadPlaylists');
        } else {
          throw result.data.error_text;
        }
      } catch (e) {
        console.error(e);
      }
    },
    async removeTrack(ctx, { playlist, track }) {
      try {
        const params = {};

        params.track_id = track;

        if (playlist) {
          params.playlist_id = playlist;
        }

        console.log(params);

        const result = await axios.post(config.apiURL + '/streaming/remove-track/', params, {
          headers: {
            'Authorization': `JWT ${ctx.state.token}`
          }
        });

        if (!result.data.error) {
          ctx.dispatch('loadPlaylists');
        } else {
          throw result.data.error_text;
        }
      } catch (e) {
        console.error(e);
      }
    },
    async removePlaylist(ctx, { playlist }) {
      try {
        const result = await axios.post(config.apiURL + '/streaming/delete-playlist/', {
          playlist_id: playlist
        }, {
          headers: {
            'Authorization': `JWT ${ctx.state.token}`
          }
        });

        if (!result.data.error) {
          ctx.dispatch('loadPlaylists');
        } else {
          throw result.data.error_text;
        }
      } catch (e) {
        console.error(e);
      }
    },
    async signup(ctx, payload) {
      try {
        const result = await axios.post(config.apiURL + '/signup/', payload);
        console.log(result);

        if (result.data && result.data.token) {
          ctx.commit('setToken', { token: result.data.token });
          ctx.commit('setUser', { user: result.data.user_data });
          ctx.dispatch('loadPlaylists');

          Cookies.set('token', result.data.token);
        }
      } catch (e) {
        console.error(e);
      }
    },
    async login(ctx, payload) {
      try {
        const result = await axios.post(config.apiURL + '/token-auth/', payload);

        ctx.commit('setToken', { token: result.data.token });
        ctx.commit('setUser', { user: result.data.user_data });
        ctx.dispatch('loadPlaylists');
        
        Cookies.set('token', result.data.token);
      } catch (e) {
        console.error(e);
      }
    },
    async verify(ctx) {
      const token = Cookies.get('token');

      if (token) {
        try {
          const result = await axios.post(config.apiURL + '/token-verify/', { token });

          ctx.commit('setToken', { token });
          ctx.commit('setUser', { user: result.data.user_data });
          ctx.dispatch('loadPlaylists');

        } catch (e) {
          console.error(e);
        }
      }
    },
    async logout(ctx) {
      ctx.commit('setToken', { token: null });
      ctx.commit('setUser', { user: null });
      ctx.commit('setPlaylists', { playlists: [] });

      Cookies.erase('token');
    },
  }
}