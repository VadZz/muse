import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'

import App from './App.vue'

import router from './router'
import store from './store'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// This imports all the layout components such as <b-container>, <b-row>, <b-col>:
import { Layout } from 'bootstrap-vue/es/components'
Vue.use(Layout)

// This imports <b-modal> as well as the v-b-modal directive as a plugin:
import { Modal } from 'bootstrap-vue/es/components'
Vue.use(Modal)

// This imports <b-card> along with all the <b-card-*> sub-components as a plugin:
import { Card } from 'bootstrap-vue/es/components'
Vue.use(Card)

Vue.use(BootstrapVue)

Vue.config.productionTip = false

const app = new Vue({
  router,
  store,
  render: h => h(App)
});

app.$mount('#app');

(async () => {
  await app.$store.dispatch('auth/verify');
})();
